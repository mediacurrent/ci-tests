import type { Options } from '@wdio/types';
const headless: boolean = process.env.HEADLESS === 'true';

const browserOptions = {
    args: headless
        ? ['--disable-web-security', '--headless', '--disable-dev-shm-usage', '--no-sandbox', '--window-size=1920,1080']
        : ['--disable-web-security', '--disable-dev-shm-usage', '--no-sandbox', '--start-maximized']
};

export const dynamicConfig: Options.Testrunner = {
    capabilities: [
        {
            browserName: 'chrome',
            browserVersion: 'stable',
            'goog:chromeOptions': browserOptions,
            acceptInsecureCerts: true
        }
    ]
};