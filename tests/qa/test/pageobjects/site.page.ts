import { $ } from '@wdio/globals'

import Page from './page.js';

/**
 * sub page containing specific selectors and methods for generic page
 */
class SitePage extends Page {
    /**
   * Define selectors using getter methods.
   */
    public get mainHeading () {
        return $('h1');
    }

    public get mainFooter () {
        return $('footer');
    }

    public get siteHeader () {
        return $('header');
    }

    public get siteNav () {
        return $('nav');
    }

    public get firstMainNav () {
        return this.siteNav.$('nav > ul > li:nth-of-type(1) button');
    }

    public get firstSubNav () {
        return this.siteNav.$('nav > ul > li:nth-of-type(1) div > ul > li:nth-of-type(1) > a');
    }

    public get secondMainNav () {
        return this.siteNav.$('nav > ul > li:nth-of-type(2) button');
    }

    public get secondSubNav () {
        return this.siteNav.$('nav > ul > li:nth-of-type(2) div > ul > li:nth-of-type(2) > a');
    }

    public get thirdMainNav () {
        return this.siteNav.$('nav > ul > li:nth-of-type(3) button');
    }

    public get thirdSubNav () {
        return this.siteNav.$('nav > ul > li:nth-of-type(3) div > ul > li:nth-of-type(3) > a');
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    public open (path: string) {
        return super.open(path);
    }
}

export default new SitePage();
