import { $ } from '@wdio/globals'

import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class SecurePage extends Page {
    /**
     * define selectors using getter methods
     */
    public get adminToolbar () {
        return $('#toolbar-administration');
    }

    public get titleField () {
        return $('#edit-title-wrapper');
    }

    public get titleFieldInput () {
        return this.titleField.$('input');
    }

    public get contentTabs () {
        return $('section.tabs');
    }

    public get deleteTab () {
        return this.contentTabs.$('aria/Delete');
    }

    public get saveButton () {
        return $('input.button--primary[value="Save"]');
    }

    public get btnSubmit () {
        return $('#edit-submit');
    }

    public get pageMessages () {
        return $('div.messages');
    }
}

export default new SecurePage();
