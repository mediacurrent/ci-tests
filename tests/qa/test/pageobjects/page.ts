import {browser } from '@wdio/globals'

import {E2E_URL} from '../../utils/constants.utils.ts';

/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
export default class Page {
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
    public open (path: string) {
        return browser.url(`${E2E_URL}/${path}`)
    }

    public openHome () {
        return browser.url(`${E2E_URL}`)
    }
}
