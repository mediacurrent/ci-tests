import { $ } from '@wdio/globals'

import {E2E_PASSWORD, E2E_USERNAME} from '../../utils/constants.utils.ts';
import Page from './page.js';

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    /**
     * define selectors using getter methods
     */
    public get inputUsername () {
        return $('#edit-name');
    }

    public get inputPassword () {
        return $('#edit-pass');
    }

    public get btnSubmit () {
        return $('#edit-submit');
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    public async login () {
        await this.inputUsername.setValue(E2E_USERNAME);
        await this.inputPassword.setValue(E2E_PASSWORD);
        await this.btnSubmit.click();
    }

    /**
     * overwrite specific options to adapt it to page object
     */
    public open () {
        return super.open('user');
    }

    public async logout () {
        return super.open('user/logout');
    }
}

export default new LoginPage();
