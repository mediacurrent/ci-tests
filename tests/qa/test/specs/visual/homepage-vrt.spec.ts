import allureReporter from '@wdio/allure-reporter';
import { expect } from '@wdio/globals'

import SitePage from '../../pageobjects/site.page.ts'

describe('VRT, Homepage:', () => {

    before(() => {
        SitePage.openHome()
    });

    it('Check for Header', async () => {
        allureReporter.addFeature('Header Check')
        await expect(SitePage.siteHeader).toBeExisting()
        await expect(SitePage.siteHeader).toMatchElementSnapshot('siteHeader', 7)
    })
    it('Check for Nav', async () => {
        allureReporter.addFeature('Nav Check')
        await expect(SitePage.siteNav).toBeExisting()
        await expect(SitePage.siteNav).toMatchElementSnapshot('siteNav', 7)
    })
    it('Check Entire Page', async () => {
        allureReporter.addFeature('Page VRT')
        // Check a full page screenshot match with baseline
        await expect(browser).toMatchFullPageSnapshot('fullHomepage', 5)
    })
    it('Check Page Layout', async () => {
        allureReporter.addFeature('Page Layout')
        // Check a full page screenshot with options for `checkFullPageScreen` command
        await expect(browser).toMatchFullPageSnapshot('fullHomepageLayout', 5, {
            enableLayoutTesting: true
        })
    })
})