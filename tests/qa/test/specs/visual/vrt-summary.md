# Visual Regression Testing Summary

Visual regression tests check for unintended visual changes on a site by comparing current screenshots to a baseline version. Limiting these tests to key pages or components helps ensure that critical visual elements, such as layout, design, and user interface, remain consistent. Running these tests on every deployment helps catch subtle design or rendering issues early, maintaining the overall look and feel of the site across updates.

### Summary of Critical Path Tests

Below are list of the pages and components tested visually. This list should remain to be around the top 10 to 20 critical pages and components.

* Homepage - [Homepage VRT](homepage-vrt.spec.ts)
* TODO......

### Running Visual Regression Tests

VRTs are best run twice where we establish the baseline first and then compare another branch or environment with our baseline. The baseline should generally be established from Prod and then VRts can be run against another environment.

From the testing directory (`cd ./tests/qa`):

To establish the baseline: `npm run wdio:vrt-baseline`

To run the VRT after baseline: `npm run wdio:vrt`

### Environment controls

All webdriver commands are set up to receive an environment command to control which environment the test will run on.
Assuming your .env file is set up with the appropriate URLs, add the `--Env=` flag to your wdio commands to control the environment.

Example:

- `npm run wdio:vrt -- --Env=dev`

Appropriate flags are:

* dev
* test
* local

By default, if no environment is specified, then "local" is used.

### Visual Regression Reporting
Once we have run a VRT, we can then report on it. This is based on JSON output that is created when the test is run. Once VRTs are run:

To install the report: `npm run vrt-report:install`

To run the VRT after baseline: `npm run vrt-report:run`

#### Main README

```sh

```

To learn more about how to run the testing, please check out the main WebdriverIO testing [README](../../../README.md)