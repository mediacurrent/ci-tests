import allureReporter from '@wdio/allure-reporter';
import { expect } from '@wdio/globals'

import LoginPage from '../../pageobjects/login.page.ts'
import SecurePage from '../../pageobjects/secure.page.ts'

describe('Critical Path, User Login:', () => {
    it('Log in with Credentials', async () => {
        allureReporter.addFeature('Open User Login Page')
        await LoginPage.open()
        await LoginPage.login()
    })
    it('Check Admin Toolbar', async () => {
        allureReporter.addFeature('Admin Toolbar Check')
        await expect(SecurePage.adminToolbar).toBeExisting()
        await expect(SecurePage.adminToolbar).toHaveText(expect.stringContaining('Content'))
        await expect(SecurePage.adminToolbar).toMatchElementSnapshot('adminToolbar', 7)
    })
    it('Logout with user', async () => {
        allureReporter.addFeature('User Logout')
        await LoginPage.logout()
    })
})
