import allureReporter from '@wdio/allure-reporter'
import { browser,expect } from '@wdio/globals'

import LoginPage from '../../pageobjects/login.page.ts'
import SecurePage from '../../pageobjects/secure.page.ts'

describe('Critical Path, Content Creation:', () => {
    it('Log in with Credentials', async () => {
        allureReporter.addFeature('User Login Page')
        await LoginPage.open()

        await LoginPage.login()
    })
    it('Create new Page', async () => {
        allureReporter.addFeature('Adding A Page')
        await SecurePage.open('node/add/page')
        // TODO: Replace with waitforexists
        await browser.pause(3000)
        await expect(SecurePage.titleField).toBeExisting()

        await SecurePage.titleFieldInput.setValue('Test Page for Smoke Test')
        await SecurePage.saveButton.click()
    })
    it('Check Creation Success', async () => {
        allureReporter.addFeature('Page Added Messaging')
        // TODO: Replace with waitforexists
        await browser.pause(3000)
        // Check for status message.
        await expect(SecurePage.pageMessages).toBeExisting()
        await expect(SecurePage.pageMessages).toHaveText(expect.stringContaining('has been created.'))
        await expect(SecurePage.pageMessages).toMatchElementSnapshot('basicPageMessages', 7)
    })
    it('Delete the page', async () => {
        allureReporter.addFeature('Delete Test Page')
        await expect(SecurePage.contentTabs).toBeExisting()
        await expect(SecurePage.contentTabs).toHaveText(expect.stringContaining('Delete'))

        await SecurePage.deleteTab.click()
        await SecurePage.btnSubmit.click()
        await expect(SecurePage.pageMessages).toBeExisting()
        await expect(SecurePage.pageMessages).toHaveText(expect.stringContaining('has been deleted.'))
    })
})
