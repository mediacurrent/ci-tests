import allureReporter from '@wdio/allure-reporter';
import { expect } from '@wdio/globals'

import SitePage from '../../pageobjects/site.page.ts'

describe('Critical Path, Search:', () => {
    it('Open Search', async () => {
        allureReporter.addFeature('Open Search Page')
        await SitePage.open('search?search=human')
    })
    it('Check for Heading', async () => {
        allureReporter.addFeature('Header check')
        await expect(SitePage.mainHeading).toBeExisting()
        await expect(SitePage.mainHeading).toHaveText(expect.stringContaining('Results'))
        await expect(SitePage.mainHeading).toMatchElementSnapshot('mainSearchHeading', 7)
    })
    it('Check for Nav', async () => {
        allureReporter.addFeature('Navigation Check')
        await expect(SitePage.siteNav).toBeExisting()
        await expect(SitePage.siteNav).toMatchElementSnapshot('siteNav', 7)
    })
})
