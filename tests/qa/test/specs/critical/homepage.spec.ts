import allureReporter from '@wdio/allure-reporter';
import { expect } from '@wdio/globals'

import SitePage from '../../pageobjects/site.page.ts'

describe('Critical Path, Homepage:', () => {
    it('Open Homepage', async () => {
        allureReporter.addFeature('Open Homepage')
        await SitePage.openHome()
    })
    it('Check for Header', async () => {
        allureReporter.addFeature('Header Check')
        await expect(SitePage.siteHeader).toBeExisting()
        await expect(SitePage.siteHeader).toMatchElementSnapshot('siteHeader', 7)
    })
    it('Check for Nav', async () => {
        allureReporter.addFeature('Nav Check')
        await expect(SitePage.siteNav).toBeExisting()
        await expect(SitePage.siteNav).toMatchElementSnapshot('siteNav', 7)
    })
})
