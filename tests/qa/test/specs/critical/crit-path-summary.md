# Critical Path Testing Summary

Critical paths are the most essential user journeys or workflows that a site must support flawlessly to function as intended. Limiting the number to around five ensures focus on the most important areas, keeping testing manageable while covering the key features. Running these tests for every change helps catch any potential issues early, ensuring the critical functionality remains stable and reliable across updates.

### Summary of Critical Path Tests

Below is a list of what our application is testing for its critical paths:

* Homepage (Can a user hit the homepage and see the most critical elements) - [Homepage Spec](homepage.spec.ts)
* Login (Can an anonymous user login into the site) - [Login Spec](login.spec.ts)
* Navigate (Can a user click on links in the navigation and explore the site) - [Navigate Spec](navigate.spec.ts)
* Search (Can a user utilize the search functionality of the site) - [Search Spec](search.spec.ts)
* Create Content (Can an admin login, create a basic page, and successfully delete it) - [Create Page Spec](create-page.spec.ts)

### Running Critical Path Test

From the testing directory (```cd ./tests/qa```):

```npm run wdio:critical```

### Environment controls
All webdriver commands are set up to receive an environment command to control which environment the test will run on.
Assuming your .env file is set up with the appropriate URLs, add the ```--Env=``` flag to your wdio commands to control the environment.

Example:
- ```npm run wdio:critical -- --Env=dev```

Appropriate flags are:

* dev
* test
* local

By default, if no environment is specified, then "local" is used.

#### Main README

To learn more about how to run the testing, please check out the main WebdriverIO testing [README](../../../README.md)