import allureReporter from '@wdio/allure-reporter';
import { expect } from '@wdio/globals'

import SitePage from '../../pageobjects/site.page.ts'

describe('Critical Path, Spot Navigate:', () => {
    it('Open Homepage', async () => {
        allureReporter.addFeature('Open Homepage')
        await SitePage.openHome()
    })
    it('Check for Nav', async () => {
        allureReporter.addFeature('Navigation Check')
        await expect(SitePage.siteNav).toBeExisting()
        await expect(SitePage.siteNav).toMatchElementSnapshot('siteNav', 7)
    })
    it('Click First Link', async () => {
        allureReporter.addFeature('First Link Click')
        await SitePage.firstMainNav.click()
        await SitePage.firstSubNav.isClickable()
        await SitePage.firstSubNav.click()
        await expect(SitePage.siteNav).toBeExisting()
    })
    it('Click Second Link', async () => {
        allureReporter.addFeature('Second Link Click')
        await SitePage.secondMainNav.click()
        await SitePage.secondSubNav.isClickable()
        await SitePage.secondSubNav.click()
        await expect(SitePage.siteNav).toBeExisting()
    })
})
