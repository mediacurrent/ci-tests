import yargs from 'yargs';

const arguments_: any = yargs(process.argv.slice(2)).argv;

class Arguments {
    getArgumentValue = async (parameter: string) => {
        return arguments_[`${parameter}`];
    };
}
export default new Arguments();
