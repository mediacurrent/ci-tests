import report from '@wdio/allure-reporter'
import { ChainablePromiseArray, ChainablePromiseElement, ElementArray } from 'webdriverio';

export const addLog = (log:string) => {
    report.addStep(`STEP: ${log}`)
    console.log(`STEP: ${log}`)
}

export const selectDropdown = async(elements:ChainablePromiseArray<ElementArray>, value:string)=> {
    const elementArray = await elements;
    for (const [index, elementObject] of elementArray.entries()) {
        const element = await elementObject.getAttribute('value');
        if (element === value) {
            await elementArray[index].click()
            addLog(`Selected dropdown value: ${value}`)
            break;
        }
    }
}

export const setText = async(element: ChainablePromiseElement<Promise<WebdriverIO.Element>>, text:string)=> {
    await element.setValue(text)
    addLog(`Entered value: ${text}`)
}

export const selectVisibleText = async(element:  ChainablePromiseElement<Promise<WebdriverIO.Element>>, text:string)=> {
    await element.selectByVisibleText(text)
    addLog(`Selected by visible text: ${text}`)
}

export const click = async(element: ChainablePromiseElement<Promise<WebdriverIO.Element>>) => {
    await element.click()
    addLog(`Clicked on element: ${await element.selector}`)
}
