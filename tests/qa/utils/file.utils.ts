// @ts-expect-error necessary import
import fs from 'fs'

// @ts-expect-error necessary import
import { addLog } from './commands.utils.ts';

export const parseJsonFile = (datapath:string)=> {
    const data = fs.readFileSync(datapath, 'utf8');
    return JSON.parse(data)
}

export const deleteDirectory = (path: string)=> {
    if(fs.existsSync(path)) {
        fs.rmSync(path, {recursive: true})
        addLog(`Directory Deleted: ${path}`)
    }
}
