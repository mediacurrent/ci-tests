import Arguments from '../utils/arguments.utils.ts';

// User authentication.
export const E2E_USERNAME = process.env.TEST_USERNAME;
export const E2E_PASSWORD = process.env.TEST_PASSWORD;

// Define type for list of possible domains.
type DomainList = {
    [key: string]: string;
};

// Build out list of domains from config.
const domains: DomainList = {
    local: process.env.ENV_LOCAL_URL ?? 'https://example.com',
    dev: process.env.ENV_DEV_URL ?? 'https://dev.example.com',
    test: process.env.ENV_TEST_URL ?? 'https://stage.example.com',
    prod: process.env.ENV_PROD_URL ?? 'https://example.com',
};

// Logic to choose the baseurl based on environment.
const environment: string = await Arguments.getArgumentValue('Env') || 'local';

// Pass authentication if needed, returns correct domain.
export const E2E_URL = setFinalUrl(domains, environment);
console.log('Environment: ' + environment);

/**
 * Adds Basic Authentication string to domain for non local environments
 * Will not add authentication if values are uset in .env
 *
 * @param {DomainList} domains Keyed list of domains
 * @param {string} env Selected environment
 * @returns {string}
 */
function setFinalUrl(domains: DomainList, environment_: string): string {
    // Basic Authentication credentials.
    const basicUsername = process.env.ENV_BASICAUTH_USERNAME;
    const basicPassword = process.env.ENV_BASICAUTH_PASSWORD;
    let selectedDomain = domains[environment_];

    // Check if credentials are set and env is not local.
    if (basicUsername && basicPassword && environment_ !== 'local') {
        const basicCreds = `${basicUsername}:${basicPassword}`;

        if (selectedDomain.includes('http://')) {
            selectedDomain = `http://${basicCreds}@${selectedDomain.slice(7)}`;
        } else if (selectedDomain.includes('https://')) {
            selectedDomain = `https://${basicCreds}@${selectedDomain.slice(8)}`;
        } else {
            selectedDomain = `https://${basicCreds}@${selectedDomain}`;
        }
    }

    return selectedDomain;
}
